Generating the server cert:
```
keytool -genkeypair -alias server-cert -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore server-cert.p12 -validity 3650
```

Generating the client cert:
```
keytool -genkeypair -alias client-cert -keyalg RSA -keysize 2048 -storetype PKCS12 -keystore client-cert.p12 -validity 3650
```

Export public certificate file from client cert:
```
keytool -export -alias client-cert -file client-cert.crt -keystore client-cert.p12
```

Import client cert into server cert:
```
keytool -import -alias client-cert -file client-cert.crt -keystore server-cert.p12
```

Invoking Actuator endpoint via HTTP:
```
curl http://localhost:8066/actuator/health
```

Should return:
```
{"status":"UP"}
```

Invoking non-Actuator via HTTPS:
```
curl --insecure --cert-type P12 --cert client-cert.p12:client-cert https://localhost:8065/api/test
```

Should return:
```
Heyyyyy youuuuu guyyyyyysssss!
```